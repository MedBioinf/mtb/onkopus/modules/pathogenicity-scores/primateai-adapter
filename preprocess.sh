#
# tabix file generation for PrimateAI score files
#

DB_PATH="${1}"
DB_FILE="${DB_PATH}/${2}"
DB_FILE_UNZIP="${DB_PATH}/${3}"
DB_FILE_UNZIP_SORTED="${DB_FILE_UNZIP}.sorted"

gunzip "${DB_FILE}"
# remove empty line in DB files
sed -i '/^$/d' "${DB_FILE_UNZIP}"
# sort lines
sort -k1,1 -k2,2n -o "${DB_FILE_UNZIP_SORTED}" "${DB_FILE_UNZIP}"
sed -i -e '/chr\tpos\tref\talt\trefAA\taltAA\tstrand_1pos_0neg\ttrinucleotide_context\tUCSC_gene\tExAC_coverage\tprimateDL_score/s/^/#/g' "${DB_FILE_UNZIP_SORTED}"
sed -i -e 's/chr//g' "${DB_FILE_UNZIP_SORTED}"
mv -v "${DB_FILE_UNZIP_SORTED}" "${DB_FILE_UNZIP}"
bgzip "${DB_FILE_UNZIP}"

# generate tabix file
tabix -s 1 -b 2 -e 2 -p vcf "${DB_FILE}"
