# PrimateAI-Adapter

Adapter for retrieving PrimateAI pathogenicity scores [1] of missense variants.

### Run locally

### Build and run Docker container

### Build from source

#### Preprocess database files

Preprocess the database files by running the script preprocess.sh. The script receives 3 command line parameters: 1) The file path to the database files, 2) the database file and 3) the database file name of the unzipped file, where 3) is the same file name without the '.gz' suffix

A sample database preprocessing has the following format:
```
./preprocess.sh /path/to/database/files PrimateAI_scores_v0.2.tsv.gz PrimateAI_scores_v0.2.tsv
./preprocess.sh /path/to/database/files PrimateAI_scores_v0.2_hg38.tsv.gz PrimateAI_scores_v0.2_hg38.tsv
```

#### References

[1] Sundaram, L., Gao, H., Padigepati, S. R., McRae, J. F., Li, Y., Kosmicki, J. A., ... & Farh, K. K. H. (2018). Predicting the clinical impact of human mutation with deep neural networks. Nature genetics, 50(8), 1161-1170.

