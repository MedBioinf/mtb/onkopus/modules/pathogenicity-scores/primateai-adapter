FROM python:3.11.0rc2-bullseye

RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get install -y python3-pip python3-dev tabix

RUN python3 -m pip install --upgrade pip
RUN python3 -m venv /opt/venv
RUN /opt/venv/bin/python3 -m pip install --upgrade pip

WORKDIR /data
RUN chmod 777 /data

WORKDIR /app/primateai-adapter
COPY ./requirements.txt /app/primateai-adapter/requirements.txt
RUN /opt/venv/bin/pip install -r requirements.txt

COPY . /app/primateai-adapter/
CMD ["export", "PYTHONPATH=/app"]

EXPOSE 10120

CMD [ "/opt/venv/bin/python3", "/app/primateai-adapter/app.py" ]
