from flask import jsonify, request
import traceback, os
from flask_cors import CORS
from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint
import conf.read_config as config
import re, subprocess

DEBUG = True
SERVICE_NAME="primateai-adapter"
VERSION="v1"

app = Flask(__name__)
app.config.from_object(__name__)

CORS(app, resources={r'/*': { 'origins': '*' }})
SWAGGER_URL = f'/{SERVICE_NAME}/{VERSION}/docs'
API_URL = '/static/config.json'

swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'app_name': "PrimateAI-Adapter"
    },
)

# definitions
SITE = {
        'logo': 'FLASK-VUE',
        'version': '0.0.1'
}


@app.route(f'/{SERVICE_NAME}/{VERSION}/<genome_version>/full', methods=['GET'])
def get_score(genome_version=None):
    variant = request.args.get("genompos")
    variant_list = variant.split(",")

    genome_pos_exp = "(chr)([0-9|X]+):([0-9]+)([A|G|C|T]+)>([A|G|C|T]+)"
    p = re.compile(genome_pos_exp)

    response = {}
    tabix_q = ""
    for variant in variant_list:
        response[variant] = {}

        try:
            groups = p.match(variant)
            if groups is not None:
                chrom = groups.group(2)
                pos = groups.group(3)
                ref = groups.group(4)
                alt = groups.group(5)
                tabix_q += chrom + ":" + pos + "-" + pos + " "
                #print(chrom, ":", pos)
                #print(tabix_q)
        except:
            print("Error parsing variant: ",variant,": ",traceback.format_exc())

    db_file = None
    if (genome_version == 'hg19') or (genome_version == 'GRCh37'):
        db_file = config.__DATABASE_PATH__ + "/" + config.__DATABASE_FILE_HG19__
    elif (genome_version == 'hg38') or (genome_version == 'GRCh38'):
        db_file = config.__DATABASE_PATH__ + "/" + config.__DATABASE_FILE_HG38__


    tabix_q = "tabix " + db_file + " " + tabix_q
    #print(tabix_q)
    process = subprocess.Popen(tabix_q.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    output = output.decode("utf-8")
    #print("result: ",output)
    results = output.split('\n')
    for result in results:
        elements = result.split("\t")
        #print("elements ",elements)
        if len(elements)>1:
            chrom_res = elements[0]
            pos_res = elements[1]
            ref_res = elements[2]
            alt_res = elements[3]
            var = "chr" + chrom_res + ":" + pos_res + ref_res + ">" + alt_res
            if var in variant_list:
                #print("result: ",result)

                score = float(elements[10])
                score = round(score, 4)

                response[var]["Score"] = score

    #print("return ",response)
    return response

if __name__ == '__main__':
    app.register_blueprint(swaggerui_blueprint)
    app.run(host='0.0.0.0', debug=True, port=10120)
