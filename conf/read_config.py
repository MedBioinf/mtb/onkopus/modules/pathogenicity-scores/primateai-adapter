import os, configparser

config = configparser.ConfigParser()
config.read(os.path.join(os.path.dirname(__file__), '', 'config.ini'))

if "DATABASE_PATH" in os.environ:
    __DATABASE_PATH__ = os.getenv("DATABASE_PATH")
else:
    __DATABASE_PATH__ = config['DEFAULT']['DATABASE_PATH']

if "DATABASE_FILE_HG19" in os.environ:
    __DATABASE_FILE_HG19__ = os.getenv("DATABASE_FILE_HG19")
else:
    __DATABASE_FILE_HG19__ = config['DEFAULT']['DATABASE_FILE_HG19']

if "DATABASE_FILE_HG38" in os.environ:
    __DATABASE_FILE_HG38__ = os.getenv("DATABASE_FILE_HG38")
else:
    __DATABASE_FILE_HG38__ = config['DEFAULT']['DATABASE_FILE_HG38']

